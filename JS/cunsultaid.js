function getAllAlbums() {
    const xhr = new XMLHttpRequest();
  
    xhr.open('GET', 'https://jsonplaceholder.typicode.com/albums', true);
  
    xhr.onload = function () {
      if (xhr.status === 200) {
        const albums = JSON.parse(xhr.responseText);
        displayAlbums(albums);
      } else {
        console.error('Error:', xhr.status);
      }
    };
  
    xhr.send();
  }
  
  function getAlbumById() {
    const albumId = document.getElementById('albumId').value;
  
    if (!albumId) {
      alert('Por favor, ingrese un ID válido.');
      return;
    }
  
    const xhr = new XMLHttpRequest();
  
    xhr.open('GET', `https://jsonplaceholder.typicode.com/albums/${albumId}`, true);

  
    xhr.onload = function () {
      if (xhr.status === 200) {
        const album = JSON.parse(xhr.responseText);
        displayAlbum(album);
      } else if (xhr.status === 404) {
        alert('Álbum no encontrado.');
      } else {
        console.error('Error:', xhr.status);
      }
    };
  
    xhr.send();
  }
  
  function displayAlbums(albums) {
    const outputDiv = document.getElementById('output');
    outputDiv.innerHTML = '';
  
    albums.forEach(album => {
      const albumDiv = document.createElement('div');
      albumDiv.innerHTML = `
        <strong>ID del Álbum:</strong> ${album.id} <br>
        <strong>Título del Álbum:</strong> ${album.title} <br>
        <hr>
      `;
      outputDiv.appendChild(albumDiv);
    });
  }
  
  function displayAlbum(album) {
    const outputDiv = document.getElementById('output');
    outputDiv.innerHTML = '';
  
    const albumDiv = document.createElement('div');
    albumDiv.innerHTML = `
      <strong>ID del Álbum:</strong> ${album.id} <br>
      <strong>Título del Álbum:</strong> ${album.title} <br>
      <hr>
    `;
    outputDiv.appendChild(albumDiv);
  }
