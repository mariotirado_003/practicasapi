function getAllUsers() {
    const xhr = new XMLHttpRequest();
  
    xhr.open('GET', 'https://jsonplaceholder.typicode.com/users', true);
  
    xhr.onload = function () {
      if (xhr.status === 200) {
        const users = JSON.parse(xhr.responseText);
        displayUsers(users);
      } else {
        console.error('Error:', xhr.status);
      }
    };
  
    xhr.send();
  }
  
  function getUserById() {
    const userId = document.getElementById('userId').value;
  
    if (!userId) {
      alert('Por favor, ingrese un ID válido.');
      return;
    }
  
    const xhr = new XMLHttpRequest();
  
    xhr.open('GET', `https://jsonplaceholder.typicode.com/users/${userId}`, true);
  
    xhr.onload = function () {
      if (xhr.status === 200) {
        const user = JSON.parse(xhr.responseText);
        displayUser(user);
      } else if (xhr.status === 404) {
        alert('Usuario no encontrado.');
      } else {
        console.error('Error:', xhr.status);
      }
    };
  
    xhr.send();
  }
  
  function displayUsers(users) {
    const outputDiv = document.getElementById('output');
    outputDiv.innerHTML = '';
  
    users.forEach(user => {
      const userDiv = document.createElement('div');
      userDiv.innerHTML = `
        <strong>ID:</strong> ${user.id} <br>
        <strong>Nombre:</strong> ${user.name} <br>
        <strong>Email:</strong> ${user.email} <br>
        <strong>Username:</strong> ${user.username} <br>
        <hr>
      `;
      outputDiv.appendChild(userDiv);
    });
  }
  
  function displayUser(user) {
    const outputDiv = document.getElementById('output');
    outputDiv.innerHTML = '';
  
    const userDiv = document.createElement('div');
    userDiv.innerHTML = `
      <strong>ID:</strong> ${user.id} <br>
      <strong>Nombre:</strong> ${user.name} <br>
      <strong>Email:</strong> ${user.email} <br>
      <strong>Username:</strong> ${user.username} <br>
      <hr>
    `;
    outputDiv.appendChild(userDiv);
  }